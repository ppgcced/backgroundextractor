#include <stdio.h>
#include <stdlib.h>
#include "linkedList2.h"

struct lnode *initNode(int data) //inicia um novo nó
{
    struct lnode *node = malloc(sizeof(struct lnode)); //aloca memoria para o novo nó
    node->next = NULL; //deixa o proximo item
    node->str = data; //adiciona o valor passado por parametro em str
    return node;
}

void *insert(char *data, struct lnode *list)
{
    struct lnode *p = initNode(data); //inicia um novo nó
    struct lnode *q = NULL; //declara um nó auxiliar

    if((list == NULL) || (list->str == data) > 0) //verifica se deve inserir no inicio
    {
        p->next = list; //adiciona o resto da lista aos proximos itens
    }
    else
    {
        q = list;
        while((q->next != NULL && q->next->str == data) < 0) //busca onde o novo nó será adicionado
        {
            q = q->next;
        }
        p->next = q->next; //inverte os nós
        q->next = p;
    }
}

void free_list(struct lnode *list)
{
    struct lnode *p = list; //cria um nó para receber o enviado por parâmetro
    while (NULL != p)
    {
        struct lnode *tmp = p->next; //cria um nó auxiliar para receber o proximo item
        p->str = NULL; //limpa o valor de str
        free(p); //libera a memoria de p
        p = tmp; //p recebe o proximo item
    }

}

void print_list(struct lnode *list)
{
    struct lnode *p = malloc(sizeof(struct lnode)); //aloca memoria para o nó

    for(p = list; p != NULL; p = p->next) //imprime o valor de str enquanto o item atual não estiver vazio
        printf("%c", *(p->str));
}

int getLength(struct lnode *linkedList)
{
    int counter = 0; //inicia o contador
    struct lnode *p; //cria um auxiliar que recebe o valor enviado por parametro
    p = linkedList;
    while (p != NULL)//aumenta o contador enquanto o item não está vazio
    {
        counter++;
        p = p->next; //recebe o proximo item da lista
    }
    return counter;
}

char getByIndex(struct lnode *linkedList, int idx)
{
    int counter = 0; //inicia o contador
    struct lnode *p; //cria um auxiliar
    p = linkedList; //auxiliar recebe o parametro
    while (p != NULL && counter < idx) //enquanto o contador for menor do que o valor passado por parametro
    {
        p = p->next; //o auxiliar recebe o proximo item
        counter++;
    }
    return *(p->str);
}

void sortedInsert(struct lnode **headRef, struct lnode *newNode)
{
    struct lnode **current = headRef; //recebe o nó enviado por parametro

    while(*current && (*current)->str < newNode->str) //enquanto o valor de current for menor que o valor de newNode
    {
        current = &((*current)->next); //recebe o proximo valor
    }
    newNode->next = *current; //o proximo valor recebe o valor atual
    *current = newNode; // o valor atual recebe o resultado ordenado
}

void insertSort(struct lnode **headRef)
{
    struct lnode *result = initNode(NULL); //cria nós auxiliares
    struct lnode *next;
    struct lnode *current;

    current = headRef[0]; //current recebe o parametro

    while(current)
    {
        next  = current->next; //next recebe o proximo valor
        sortedInsert(&result, current); //envia para ordenação
        current = next;// current o proximo item
    }
    *headRef = result; //o parametro recebe a lista ordenada

}

