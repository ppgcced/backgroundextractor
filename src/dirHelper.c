#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "dirHelper.h"

void listFilesNamesFrom(char *dirPath) //lista o nome dos arquivos
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir(dirPath);
    if (dp != NULL)
    {
        while (ep = readdir(dp))
        {
            puts(ep->d_name);
        }
        closedir(dp);
    }
    else
    {
        printf("\n%s\n", dirPath);

        perror("Não foi possível abrir diretório.");
    }
}

FILE **getFilesFrom(char *dirPath, int *filesAmount)
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir(dirPath); //abre o diretório
    if (dp != NULL)
    {
        int filesCounter = 0;
        while (ep = readdir(dp))   //conta a quantidade de arquivos
        {
            ++filesCounter;
        }
        closedir(dp);

        dp = opendir(dirPath);

        FILE **files = (FILE **) malloc(filesCounter * sizeof(FILE*)); //aloca memória para o o ponteiro de ponteiros

        filesCounter = 0;
        while (ep = readdir(dp))
        {
            if(ep->d_name[0] == '.')
            {
                continue;
            }

            char* fileFullName = malloc(strlen(dirPath) + strlen(ep->d_name)); //aloca a memória para o nome do arquivo
            strcpy(fileFullName, dirPath); // copia o nome do arquivo
            strcat(fileFullName, ep->d_name); // adiciona a extensão

            FILE *fp = fopen(fileFullName, "r+"); //abre o arquivo em modo leitura/escrita

            if (fp != NULL)
            {
                files[filesCounter++] = fp; //adiciona o ponteiro do arquivo em files
            }
            free(fileFullName); //libera a memória alocada para o fileFullName

//            fclose(fp);
        }

        *filesAmount = filesCounter; //adiciona a quantidade de arquivos

        return files;
    }
    else
    {
        printf("\n%s\n", dirPath);

        perror("Não foi possível abrir diretório.");
    }
    return NULL;
}
