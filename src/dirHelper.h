#ifndef DIR_HELPER_H_FILE

#include <stdio.h>

#define DIR_HELPER_H_FILE

void listFilesNamesFrom(char *dirPath);

FILE **getFilesFrom(char *dirPath, int *filesAmount);

#endif
