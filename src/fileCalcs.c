#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>

#include "dirHelper.h"
#include "linkedList2.h"

void skipHeader(FILE *f);

void backgroundExtractor(FILE **files, int filesAmount, FILE *fp)
{
    if(filesAmount == 0)
    {
        return;
    }

    printf("Quantidade de arquivos: %d\n", filesAmount);

    //Copia cabeçalho do primeiro arquivo
    copyHeader(files[0],fp);

    rewind((FILE*) files[0]);

    //Pula todos os cabeçalhos dos arquivos
    for (int i = 0; i < filesAmount; i++)
    {
        skipHeader((FILE*) files[i]);
    }

    int processEnded = 0;
    char pixel;
    int sizeOflist = 0;
    struct lnode *a = NULL;
    char median;
    unsigned long outCount;
    
    //Será executado até o final de um dos arquivos, ou seja, até ler todos os pixels de todos os arquivos
    while (1)
    {
        printf("Contagem: %lu\n", outCount++);
        //É criada uma lista nova por pixel de todos os arquivos
        a = initNode(NULL);

        //É buscado 1 pixel por vez de todos os arquivos e inserido na lista
        for (int i = 0; i < filesAmount; i++)
        {
            if(feof((FILE*) files[i])) //verifica se o arquivo chegou ao fim
            {
                processEnded = 1;
                break;
            }

            pixel = fgetc((FILE*) files[i]); //lê o valor do pixel

            insert(pixel, a); //insere na lista
        }

        if(processEnded == 1)
        {
            free(a); //libera a memoria alocada para a lista
            break;
        }

        sizeOflist = getLength(a); //calcula o tamanho da lista

        //ordenação da lista
        insertSort(&a);

        //busca determinado índice da lista
        median =  getByIndex(a, sizeOflist / 2);

        //insere a mediana no arquivo final
        fputc(median, fp);

        //libera memória da lista criada
        free_list(a);

        

    }
}

void copyHeader(FILE *original, FILE *created) //copia o cabeçalho do arquivo base
{
    int count = 0;
    char temp;
    while((temp = fgetc(original)) != EOF && count < 3)
    {
        fputc(temp, created);

        if(temp == '\n')
        {
            ++count;
        }
    }
}

void skipHeader(FILE *f) //pula o cabeçalho do arquivo
{
    int count = 0;
    char temp;
    while((temp = fgetc(f)) != EOF && count < 3)
    {
        if(temp == '\n')
        {
            ++count;
        }
    }
}




