#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED

struct lnode
{
    char *str;
    struct lnode *next;
};

void *insert(char *data, struct lnode *list);
void free_list(struct lnode *list);
void print_list(struct lnode *list);
int getLength(struct lnode *linkedList);
char getByIndex(struct lnode *linkedList, int idx);
void sortedInsert(struct lnode** head_ref, struct lnode* new_node);
void insertionSort(struct lnode **head_ref);
struct lnode *initNode(int data);

#endif // LINKEDLIST_H_INCLUDED
