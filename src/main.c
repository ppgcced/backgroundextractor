#include <stdlib.h>
#include <stdio.h>

#include "dirHelper.h"
#include "fileCalcs.h"

void printData(int data);
void weirdTest();
void listFilesTest();

int main(int argc, char **argv)
{
    char *dir = argc == 1 ? "./assets/vid8046/" : argv[1];
    FILE **files;
    int filesAmount = 0;
    FILE *fp = fopen("background.ppm", "wb");

    files = getFilesFrom(dir, &filesAmount);
    printf("Carregando imagens de %s\n", dir);
    //cria o arquivo final
    backgroundExtractor(files, filesAmount, fp);

    printf("Processo realizado com sucesso!\n");

    fclose(fp);

    exit(1);
}
